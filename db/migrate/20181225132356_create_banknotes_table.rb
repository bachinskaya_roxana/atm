class CreateBanknotesTable < ActiveRecord::Migration[5.1]
  def change
    create_table :banknotes, { id: false, primary_key: :nominal  } do |t|
      t.integer :nominal, index: true
      t.integer :quantity, default: 0
    end
  end
end
