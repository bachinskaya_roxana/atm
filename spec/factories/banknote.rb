FactoryBot.define do
  factory :banknote, class: Banknote do
    nominal {1}
    quantity {320}
  end
end
