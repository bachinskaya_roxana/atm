require 'rails_helper'

describe MoneyController, type: :controller do
  render_views

  it { should be_a ApplicationController }

  it { expect(post: '/money').to route_to(controller: 'money', action: 'create') }

  it { expect(patch: '/money').to route_to(controller: 'money', action: 'update') }

  describe 'create.json' do
    let!(:banknote) { stub_model ::Banknote, nominal: 1 }

    context 'when params are valid' do
      let(:params) do
        {
          banknotes: [
            {
              nominal: 1,
              quantity: 10
            }]
        }
      end

      let(:service) { ChargeService.new(params) }

      before { expect(Banknote).to receive(:find).with("1").and_return banknote }

      before { process :create, method: :post, params: params, format: :json }

      it { expect(response).to have_http_status(200) }
    end

    context 'when params is invalid' do
      let(:params) do
        {
          banknotes: [
            {
              nominal: 1
            }]
        }
      end

      before { process :create, method: :post, params: params, format: :json }

      it { expect(response).to have_http_status(422) }
    end
  end

  describe 'update.json' do
    context 'when enought money in ATM' do
      let!(:banknote_50) { create :banknote, nominal: 50, quantity: 7 }
      let!(:banknote_10) { create :banknote, nominal: 10, quantity: 3 }

      let(:params) do
        {
          total: 320
        }
      end

      let(:response_body) {
        {
          "given_money": [
              {
                  "nominal": 50,
                  "quantity": 6
              },
              {
                  "nominal": 10,
                  "quantity": 2
              }
          ]
        }
      }

      before { process :update, method: :patch, params: params, format: :json }

      it { expect(response).to have_http_status(200) }

      it { expect(response.body).to eq(response_body.to_json) }
    end

    context 'when not enought money in ATM' do
      let!(:banknote_50) { create :banknote, nominal: 50, quantity: 7 }
      let!(:banknote_10) { create :banknote, nominal: 10, quantity: 3 }

      let(:params) do
        {
          total: 420
        }
      end

      let(:error_body) {
        {
          "base": ["Not enought money in ATM"]
        }
      }

      before { process :update, method: :patch, params: params, format: :json }

      it { expect(response).to have_http_status(422) }

      it { expect(response.body).to eq(error_body.to_json) }
    end

    context 'when not exist necessery nominal in ATM' do
      let!(:banknote_50) { create :banknote, nominal: 50, quantity: 7 }
      let!(:banknote_10) { create :banknote, nominal: 10, quantity: 3 }

      let(:params) do
        {
          total: 325
        }
      end

      let(:error_body) {
        {
          "base": ["ATM doesnt have necessary banknotes to give you chosen total"]
        }
      }

      before { process :update, method: :patch, params: params, format: :json }

      it { expect(response).to have_http_status(422) }

      it { expect(response.body).to eq(error_body.to_json) }
    end

    context 'when total doesnt exist' do
      let(:error_body) {
        {
          "total": ["must be greater than 0"]
        }
      }

      before { process :update, method: :patch, params: {}, format: :json }

      it { expect(response).to have_http_status(422) }

      it { expect(response.body).to eq(error_body.to_json) }
    end
  end
end
