require 'rails_helper'

describe 'WITHDRAW MONEY' do
  let(:params) { {"total": 228} }

  let(:headers) do
    {
      'ACCEPT'            => 'application/json',
      'HTTP_CONTENT_TYPE' => 'application/json'
    }
  end

  let(:path) { '/money' }

  describe 'charge money into ATM successful case' do
    let!(:banknote_1) { create :banknote, quantity: 100 }
    let!(:banknote_25) { create :banknote, nominal: 25, quantity: 5 }
    let!(:banknote_50) { create :banknote, nominal: 50, quantity: 2 }

    before { patch path, params: params, headers: headers }

    it do
      expect(response.content_type).to eq 'application/json'

      expect(response).to have_http_status :success

      expect(response.parsed_body.fetch('given_money').fetch(0).fetch('nominal')).to eq(50)

      expect(response.parsed_body.fetch('given_money').fetch(0).fetch('quantity')).to eq(2)

      expect(response.parsed_body.fetch('given_money').fetch(1).fetch('nominal')).to eq(25)

      expect(response.parsed_body.fetch('given_money').fetch(1).fetch('quantity')).to eq(5)

      expect(response.parsed_body.fetch('given_money').fetch(2).fetch('nominal')).to eq(1)

      expect(response.parsed_body.fetch('given_money').fetch(2).fetch('quantity')).to eq(3)
    end
  end

  describe 'charge money into ATM - not enough money' do
    let!(:banknote_50) { create :banknote, nominal: 50, quantity: 2 }

    let(:params) { {"total": 300} }

    before { patch path, params: params, headers: headers }

    it do
      expect(response.content_type).to eq 'application/json'

      expect(response).to have_http_status :unprocessable_entity

      expect(response.parsed_body.fetch('base').fetch(0)).to eq("Not enought money in ATM")
    end
  end

  describe 'charge money into ATM' do
    let!(:banknote_50) { create :banknote, nominal: 50, quantity: 2 }

    let(:params) { {"total": 70} }

    before { patch path, params: params, headers: headers }

    it do
      expect(response.content_type).to eq 'application/json'

      expect(response).to have_http_status :unprocessable_entity

      expect(response.parsed_body.fetch('base').fetch(0)).to eq("ATM doesnt have necessary banknotes to give you chosen total")
    end
  end
end
