require 'rails_helper'

describe 'CHARGE MONEY iNTO ATM' do

  let!(:banknote_1) { create :banknote, quantity: 0 }
  let!(:banknote_25) { create :banknote, nominal:25, quantity: 0 }
  let!(:banknote_50) { create :banknote, nominal:50, quantity: 0 }

  let(:params) do
    {"banknotes": [{
        "nominal": 25,
        "quantity": 100
      },
      {
        "nominal": 50,
        "quantity": 100
      },
      {
        "nominal": 1,
        "quantity": 100
      }]
    }
  end

  let(:headers) do
    {
      'ACCEPT'            => 'application/json',
      'HTTP_CONTENT_TYPE' => 'application/json'
    }
  end

  let(:path) { '/money' }

  describe 'charge money into ATM' do
    before { post path, params: params, headers: headers }

    it do
      expect(response.content_type).to eq 'application/json'

      expect(response).to have_http_status :success
    end
  end

  describe 'charge money into ATM with errors successful case' do
    let(:params) do
      {"banknotes": [{
          "quantity": 100
        }]
      }
    end

    before { post path, params: params, headers: headers }

    it do
      expect(response.content_type).to eq 'application/json'

      expect(response).to have_http_status :unprocessable_entity

      expect(response.parsed_body.fetch('nominal').fetch(0)).to eq("You should pass nominal in {\"quantity\"=>\"100\"}")
    end
  end
end
