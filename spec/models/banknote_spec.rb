require 'rails_helper'

describe Banknote, type: :model do
  it { should validate_presence_of(:nominal) }

  it { should validate_inclusion_of(:nominal).in_array([1, 2, 5, 10, 25, 50]) }

  describe 'default_scope' do
    let!(:banknote_50) { create :banknote, nominal: 50, quantity: 2}

    let!(:banknote_1) { create :banknote, quantity: 0 }

    let(:collection) { described_class.all }

    it { expect(Banknote.all).to eq [banknote_1, banknote_50] }
  end

  describe 'that_existed' do
    let!(:banknote_1) { create :banknote, quantity: 0 }

    let!(:banknote_50) { create :banknote, nominal: 50, quantity: 2}

    let(:collection) { described_class.all }

    it { expect(Banknote.that_existed).to eq [banknote_50] }
  end
end
