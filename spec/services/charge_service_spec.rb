require 'rails_helper'

describe ChargeService do
  subject { ChargeService.new params }

  it { should be_a ActiveModel::Validations }

  let(:params) {
    {
      "banknotes": [
        {
          "nominal": 1,
          "quantity": 10
        }
      ]
    }
  }

  describe '#save!' do
    context 'not raise error' do
      let(:banknote_item_params) { {"nominal": 1, "quantity": 10} }

      let!(:banknote) { create :banknote }

      before { allow(subject).to receive(:banknotes_array).and_return params[:banknotes] }

      before { allow(params[:banknotes]).to receive(:each).and_yield(banknote_item_params) }

      before { allow(subject).to receive(:validate_param).with(banknote_item_params) }

      before { allow(Banknote).to receive(:find).with(banknote_item_params[:nominal]).and_return banknote }

      it { expect { subject.save! }.to_not raise_error }

      it { expect { subject.save! }.to change{banknote.quantity}.from(320).to(330) }
    end

    context 'raise error' do
      let(:params) { { "nominal": 1 } }

      let!(:banknote) { create :banknote }

      it { expect { subject.save! }.to raise_error ActiveModel::StrictValidationFailed }
    end
  end
end
