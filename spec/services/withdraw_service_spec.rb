require 'rails_helper'

describe WithdrawService do
  subject { WithdrawService.new params }

  it { should be_a ActiveModel::Validations }

  let(:params) { {"total": 319 } }

  describe '#save!' do
    context 'not raise error' do
      let(:banknote) { create :banknote }

      let(:necessary_quantity) { params[:total] / banknote.nominal }

      let(:given_money_initially) { [] }

      let(:given_money_response) { [{ nominal: 1, quantity: 319 }] }

      before { expect(subject).to receive(:get_banknote).and_return(banknote) }

      it { expect { subject.save! }.to_not raise_error }

      it { expect { subject.save! }.to change{banknote.quantity}.from(320).to(1) }

      it { expect { subject.save! }.to change{subject.given_money}.from(given_money_initially).to(given_money_response) }
    end

    context 'raise error when not enough money in ATM' do
      let(:params) { {"total": 321 } }

      let!(:banknote) { create :banknote }

      it { expect { subject.save! }.to raise_error ActiveModel::StrictValidationFailed }
    end

    context 'raise error when doesnt have necessary banknotes' do
      let(:params) { {"total": 180 } }

      let!(:banknote) { create :banknote, nominal: 50, quantity: 4 }

      it { expect { subject.save! }.to raise_error ActiveModel::StrictValidationFailed }
    end

    context 'raise error when total in params doesnt exist' do
      let(:params) { { } }

      let!(:banknote) { create :banknote, nominal: 50, quantity: 4 }

      it { expect { subject.save! }.to raise_error ActiveModel::StrictValidationFailed }
    end
  end
end
