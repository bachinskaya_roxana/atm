class ChargeService
  include ActiveModel::Validations

  attr_reader :banknotes_array

  validates :banknotes_array, presence: true

  def initialize(params)
    @banknotes_array = params[:banknotes]
  end

  def save!
    raise ActiveModel::StrictValidationFailed unless valid?

    Banknote.transaction do
      banknotes_array.each do |banknote_item|
        validate_param(banknote_item)

        banknote = Banknote.find(banknote_item[:nominal])

        banknote.quantity += banknote_item[:quantity].to_i

        banknote.save!
      end
    end
  end

  private

  def validate_param(param)
    errors.add(:nominal, "You should pass nominal in #{param}")   unless param[:nominal]
    errors.add(:quantity, "You should pass quantity in #{param}") unless param[:quantity]

    raise ActiveModel::StrictValidationFailed if errors.present?
  end
end
