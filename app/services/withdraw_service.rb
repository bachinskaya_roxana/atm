class WithdrawService
  include ActiveModel::Validations

  attr_accessor :total, :given_money

  validates :total, numericality: { only_integer: true, greater_than: 0 }
  validate :total_sum_in_ATM

  def initialize(params)
    @total       = params[:total].to_i

    @given_money = []
  end

  def save!
    raise ActiveModel::StrictValidationFailed unless valid?

    Banknote.transaction do
      while total != 0
        banknote = get_banknote

        validate_banknote_existence unless banknote

        necessary_quantity = get_necessary_quantity(banknote)

        if banknote.quantity >= necessary_quantity
          push_money_to_array(banknote.nominal, necessary_quantity)

          @total %= banknote.nominal

          banknote.update(quantity: (banknote.quantity - necessary_quantity))
        else
          push_money_to_array(banknote.nominal, banknote.quantity)

          @total -= banknote.nominal * banknote.quantity

          banknote.update(quantity: 0)
        end
      end
    end
    true
  end

  private

  def push_money_to_array(nominal, quantity)
    given_money << {nominal: nominal, quantity: quantity}
  end

  def get_banknote
    Banknote.that_existed.where('nominal <= ?', total).last
  end

  def get_necessary_quantity(banknote)
    total / banknote.nominal
  end

  # Validations
  def total_sum_in_ATM
    return unless total.presence

    sum = 0

    Banknote.all.each do |banknote|
      sum += banknote.nominal * banknote.quantity
    end

    errors.add(:base, "Not enought money in ATM") if total > sum
  end

  def validate_banknote_existence
    errors.add(:base, "ATM doesnt have necessary banknotes to give you chosen total")

    raise ActiveModel::StrictValidationFailed
  end
end
