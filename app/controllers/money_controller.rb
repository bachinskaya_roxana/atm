class MoneyController < ApplicationController
  helper_method :resource

  attr_reader :resource

  def create
    if charge_service.save!

      head :ok
    else
      render :errors, status: :unprocessable_entity
    end
  end

  def update
    render :errors, status: :unprocessable_entity unless withdraw_service.save!
  end

  private

  def charge_service
    @resource ||= ChargeService.new(params)
  end

  def withdraw_service
    @resource ||= WithdrawService.new(params)
  end
end
