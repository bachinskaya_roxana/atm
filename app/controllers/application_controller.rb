class ApplicationController < ActionController::Base

  rescue_from ActiveModel::StrictValidationFailed do |exception|
    render json: resource.errors, status: :unprocessable_entity
  end
end
