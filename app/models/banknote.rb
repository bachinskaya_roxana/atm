class Banknote < ActiveRecord::Base
  self.primary_key = "nominal"

  default_scope { order('nominal ASC') }

  scope :that_existed, -> { where.not(quantity: 0) }

  validates :nominal, presence: true, uniqueness: true
  validates :nominal, inclusion: { in: [1, 2, 5, 10, 25, 50],
    message: "%{value} is not a valid nominal value" }

end
