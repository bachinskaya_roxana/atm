# README

### ATM application

* Ruby version 2.5.3
* run `git clone https://bachinskaya_roxana@bitbucket.org/bachinskaya_roxana/atm.git`
* run `gem install bundler`
* run `bundle`
* run `rake db:create db:migrate`
* run `rake db:seed`
* run tests `rake`
* run `rails s`

### Endpoints:
#### POST `localhost:3000/money` - Charge money into cash mashine
##### required params:
*`banknotes` (array)* of objects {`nominal` (integer), `quantity` (integer)}

##### example of request
`curl -X POST "http://localhost:3000/money" -H \
"Content-Type: application/json" -d @- << EOF
{"banknotes": [
  {"nominal": 25, "quantity": 100},
  {"nominal": 50, "quantity": 100},
  {"nominal": 10, "quantity": 100}
]}
EOF`

#### PATCH `localhost:3000/money` - Withdraw money
##### required params:
*`total` (integer)*

##### example of request
`curl -X PATCH "http://localhost:3000/money" -H "Content-Type: application/json" -d '{"total": 250}'`
