Rails.application.routes.draw do
  resource :money, only: [:create, :update]
end
